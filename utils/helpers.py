import os
import datetime


def safe_filename(value):
    """Returns name that is safe to use as filename"""
    # Preserve alphanumeric characters and replace all other with underscore
    return "".join([c if c.isalnum() else "_" for c in value]).strip("_")


def take_screenshot(driver, test_name):
    """Takes screenshot and saves it as PNG file in 'screenshots' directory"""
    # Generate current timestamp and safe filename from test name
    timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    test_name = safe_filename(test_name)
    # Create 'screenshots' directory if not exists
    os.makedirs("screenshots", exist_ok=True)
    # Format and create screenshot filepath
    screenshot_path = os.path.join("screenshots", "{}_screenshot_{}.png")
    screenshot_filename = screenshot_path.format(timestamp, test_name)
    # Take screenshot
    driver.save_screenshot(screenshot_filename)
