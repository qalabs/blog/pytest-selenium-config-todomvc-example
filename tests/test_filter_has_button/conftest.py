import logging
import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

logger = logging.getLogger(__name__)


@pytest.fixture()
def filter_buttons_names(driver):
    # add dummy item to show filter
    logger.info("Adding todo item with text: 'dummy'")
    todo_input = driver.find_element(By.CLASS_NAME, "new-todo")
    todo_input.send_keys("dummy" + Keys.ENTER)

    # find element with filters
    logger.info("Looking for element with filters")
    filters = driver.find_element(By.CLASS_NAME, "filters")
    logger.info("Filters found: {}".format(bool(filters)))

    # find button elements in filters
    logger.info("Looking for filter buttons")
    filter_buttons = filters.find_elements(By.CSS_SELECTOR, "li > a")
    # create a list of button texts
    filter_buttons_names = [button.text for button in filter_buttons]
    logger.info("Filter buttons found: {}".format(filter_buttons_names))

    return filter_buttons_names
