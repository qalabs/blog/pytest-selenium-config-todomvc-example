import logging
import pytest

logger = logging.getLogger(__name__)


def idfunc(value):
    return "button:{}".format(value.lower())


@pytest.mark.parametrize("name", ["All", "Active", "Completed"], ids=idfunc)
def test_filter_has_button(filter_buttons_names, name):
    """Check if filter of list items has All button"""
    logger.info("Checking if filter has button '{}'".format(name))
    assert name in filter_buttons_names
