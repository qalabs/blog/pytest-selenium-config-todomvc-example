import os
import json
import logging

logger = logging.getLogger(__name__)


def read_test_data():
    """Reads external file with test data"""
    directory, _ = os.path.split(os.path.realpath(__file__))
    test_data_path = os.path.join(directory, "test_data.json")
    with open(test_data_path) as f:
        test_data = json.load(f)
    return test_data


def pytest_generate_tests(metafunc):
    # generate test for each unicode input for functions using text fixture
    test_data = read_test_data()
    keys, values = zip(*test_data.items())
    logger.info("Generating tests for cases: {}".format(keys))
    ids = ["type:{}".format(key) for key in keys]
    if "text" in metafunc.fixturenames:
        metafunc.parametrize("text", values, ids=ids)
