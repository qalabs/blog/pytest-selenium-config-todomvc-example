import logging
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

logger = logging.getLogger(__name__)


def test_unicode_todo_text(driver, text):
    """Check if item with unicode text was created correctly"""
    # add item with text
    logger.info("Adding todo item with text: '{}'".format(text))
    todo_input = driver.find_element(By.CLASS_NAME, "new-todo")
    todo_input.send_keys(text + Keys.ENTER)

    # find last item on list
    logger.info("Looking for todo elements to get the last item")
    todo_list = driver.find_elements(By.CSS_SELECTOR, ".todo-list li")
    last_todo_item = todo_list.pop()
    logger.info("Text for the last item found: '{}'".format(last_todo_item.text))

    logger.info("Checking if found text matches created")
    assert last_todo_item.text == text
