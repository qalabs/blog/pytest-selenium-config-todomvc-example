import os
import json
import logging
import pytest
from utils import helpers
from selenium.webdriver import Chrome
from selenium.webdriver import Firefox
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager

logger = logging.getLogger(__name__)


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    # execute all other hooks to obtain the report object
    outcome = yield
    rep = outcome.get_result()
    # set report attribute for each phase of test ("setup", "call", "teardown")
    setattr(item, "rep_" + rep.when, rep)


def pytest_addoption(parser):
    parser.addoption(
        "--browser",
        nargs='*',
        type=str.lower,
        choices=["chrome", "firefox"],
        default=["chrome"],
        help="browsers to test with (choose from 'chrome', 'firefox')"
    )
    parser.addoption(
        "--headless",
        action="store_true",
        help="enable browser headless mode"
    )
    parser.addoption(
        "--docker",
        action="store_true",
        help="indicate run in Docker container"
    )


@pytest.fixture(scope="session")
def config():
    with open("tests/config.json") as f:
        data = json.load(f)
    return data


@pytest.fixture(scope="session")
def headless(request):
    # read command line option or environment variable enabling headless mode
    return request.config.getoption("--headless") or bool(os.getenv("TEST_HEADLESS"))


@ pytest.fixture(scope="session")
def docker(request):
    # read command line option indicating run in Docker container
    return request.config.getoption("--docker")


def pytest_generate_tests(metafunc):
    # generate test for each browser defined in command line option
    browsers = metafunc.config.getoption("browser")
    logger.info("Generating tests for browsers: {}".format(browsers))
    ids = ["browser:{}".format(browser) for browser in browsers]
    if "browser" in metafunc.fixturenames:
        metafunc.parametrize("browser", browsers, ids=ids)


@pytest.fixture()
def driver(request, config, browser, headless, docker):
    """Returns browser driver with loaded TodoMVC app"""
    # initialize correct driver based on parameter value
    logger.info("Setting up '{}' browser with headless set to '{}'".format(browser, headless))
    if browser == "chrome":
        chrome_options = ChromeOptions()
        if headless:
            chrome_options.add_argument("--headless")
        if docker:
            chrome_options.add_argument("--no-sandbox")
        d = Chrome(ChromeDriverManager().install(), options=chrome_options)
    elif browser == "firefox":
        firefox_options = FirefoxOptions()
        if headless:
            firefox_options.add_argument("-headless")
        d = Firefox(executable_path=GeckoDriverManager().install(), options=firefox_options)
    d.implicitly_wait(config["wait_time"])
    d.get(config["app_url"])
    yield d
    # if test setup or execution failed take screenshot of browser
    if request.node.rep_setup.failed or request.node.rep_call.failed:
        logger.info("Taking screenshot for failed test '{}'".format(request.node.name))
        helpers.take_screenshot(d, request.node.name)
    d.quit()
